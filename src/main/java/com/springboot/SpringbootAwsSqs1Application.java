package com.springboot;

import com.springboot.controller.SQSController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.SystemPropertyUtils;

@SpringBootApplication
public class SpringbootAwsSqs1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAwsSqs1Application.class, args);
	}
}
