package com.springboot.controller;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.GetActivityTaskRequest;
import com.amazonaws.services.stepfunctions.model.GetActivityTaskResult;
import com.amazonaws.services.stepfunctions.model.SendTaskFailureRequest;
import com.amazonaws.services.stepfunctions.model.SendTaskSuccessRequest;
import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/sqs")
public class SQSController {

	private static final Logger LOG = LoggerFactory.getLogger(SQSController.class);

	@Autowired
	private QueueMessagingTemplate queueMessagingTemplate;

	@Value("${cloud.aws.end-point.uri}")
	private String sqsEndPoint;

	@GetMapping
	public void sendMessage() {
		queueMessagingTemplate.send(sqsEndPoint, MessageBuilder.withPayload("Hi this message is from local-Sunil").build());
	}
	
	@SqsListener("sunil_commands")
	public void getMessage(String message) {
		LOG.info("Message received from SQS Queue - "+message);
		JSONObject SQSMessage = new JSONObject(message);
		String token = SQSMessage.getString("TaskToken");
		LOG.info("Token Processing:::: "+token);
		try {
			updateStepFunctionStatus(token);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void updateStepFunctionStatus(String taskToken) throws InterruptedException {
		LOG.info("updating step function status");
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setSocketTimeout((int) TimeUnit.SECONDS.toMillis(70));

		AWSStepFunctions client = AWSStepFunctionsClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.withCredentials(new EnvironmentVariableCredentialsProvider())
				.withClientConfiguration(clientConfiguration)
				.build();
		JSONObject params = new JSONObject();
		params.append("output","Success from local");
		params.append("taskToken",taskToken);

		client.sendTaskSuccess(
				new SendTaskSuccessRequest().withOutput(
						params.toString()).withTaskToken(taskToken));
	}
}
